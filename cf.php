<?php

if (isset($_GET['success'])) {

    ?><p><span style="color:green;">Message successfully sent.</span> Thank you!</p><?php

} else {

    if (!empty($_POST['email']) && !empty($_POST['message'])) {

        $to = 'info@kuhn-lawfirm.com'; // your email address, can be @gmail.com, etc.
        $subject = 'Contact from kuhn-lawfirm.com/'; // change yoursite.com to your own domain name
        $message = $_POST['message']."\r\n\r\nSender IP: ".$_SERVER["REMOTE_ADDR"];
        $headers = 'From: '.$_POST['email']."\r\n".
                    'Reply-To: '.$_POST['email']."\r\n";
        mail($to, $subject, $message, $headers);

        header("Location: /cf.php?success"); // redirects the sender to success page so he or she doesn't accidentally send multiple identical messages

    } else {

        ?><p><span style="color:red;">Error detected.</span> Please make sure you have filled all fields. Press back button to go back.</p><?php

    }

}

?>
